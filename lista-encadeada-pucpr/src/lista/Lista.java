package lista;

public class Lista {

	private No primeiroNo;

	public Lista() {
		primeiroNo = null;
	}

	public No getPrimeiroNo() {
		return primeiroNo;
	}

	public boolean vazia() {
		return (primeiroNo == null);
	}

	public void inserePrimeiro(int info) {
		No noAuxiliar = new No(info);
		noAuxiliar.setInformacao(info);
		noAuxiliar.setProximoNo(primeiroNo);
		primeiroNo = noAuxiliar;
	}

	public void insereDepois(No no, int info) {
		No novoNo = new No(info);
		novoNo.setProximoNo(no.getProximoNo());
		no.setProximoNo(novoNo);
	}

	public void insereUltimo(int info) {
		if (vazia() == true) {
			inserePrimeiro(info);
		} else {
			No noAuxiliar = primeiroNo;

			while (noAuxiliar != null && noAuxiliar.getProximoNo() != null) {
				noAuxiliar = noAuxiliar.getProximoNo();
			}

			if (noAuxiliar.getProximoNo() == null) {
				No novoNo = new No(info);
				novoNo.setInformacao(info);
				noAuxiliar.setProximoNo(novoNo);
			}
		}
	}

	public No removePrimeiro() {
		if (vazia()) {
			System.out.println("Lista vazia, n�o h� o que remover.");
			return null;
		} else {
			No noAuxiliar = primeiroNo;
			primeiroNo.setProximoNo(noAuxiliar);
			return noAuxiliar;
		}

	}

	public No removeUltimo() {
		if (vazia()) {
			System.out.println("Lista vazia, n�o h� o que remover.");
			return null;
		}
		
		No noAuxiliar = primeiroNo;
		No noAnterior = null;
		
		if (noAuxiliar.getProximoNo() == null) {
			No teste = primeiroNo;
			noAuxiliar = noAuxiliar.getProximoNo();
			return teste;
		}
		
		while (noAuxiliar.getProximoNo() != null) {
			noAnterior = noAuxiliar;
			noAuxiliar = noAuxiliar.getProximoNo();
		}
		
		noAnterior.proximoNo = null;
		
		return noAnterior;
	}

	public void removeDepois(No no) {

		if (vazia()) {
			System.out.println("Lista vazia, n�o h� o que remover.");
		} else {

			No noAnterior = null;
			No noAuxiliar = primeiroNo;

			while (noAuxiliar.getProximoNo() != null) {
				noAnterior = noAuxiliar;
				noAuxiliar = noAuxiliar.getProximoNo();

				if (noAuxiliar.equals(no)) {
					noAnterior.setProximoNo(noAuxiliar.getProximoNo());
				}
			}
		}
	}

	public void mostrar() {

		if (vazia() == true) {
			System.out.println("A lista est� vazia");
		} else {
			No noAuxiliar = primeiroNo;
			while (noAuxiliar != null) {
				System.out.printf(noAuxiliar.getInformacao() + ", ");
				noAuxiliar = noAuxiliar.getProximoNo();
			}
		}
	}
}
package lista;

public class Main {

	public static void main(String[] args) {
		
		Lista lista = new Lista();
		
		lista.mostrar();

		lista.inserePrimeiro(2);
		lista.inserePrimeiro(1);

		lista.insereUltimo(4);
		lista.insereUltimo(5);
		lista.mostrar();
		System.out.println();
		
		No novoNo = lista.getPrimeiroNo().getProximoNo();
		lista.insereDepois(novoNo, 3);
		lista.mostrar();
		System.out.println();

		
		System.out.printf("\nN�mero a ser removido: " + lista.getPrimeiroNo()
		.getProximoNo().getInformacao() + "\n");
		lista.removeDepois(lista.getPrimeiroNo().getProximoNo());
		lista.mostrar();
		
		System.out.println();

		lista.removeUltimo();
		lista.mostrar();
	
	}

}

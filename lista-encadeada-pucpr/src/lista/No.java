package lista;

public class No {

	private int informacao;
	protected No proximoNo;

	
	public No(int informacao) {
		this.informacao = informacao;
		this.proximoNo = null;
	}

	public Integer getInformacao() {
		return informacao;
	}

	public void setInformacao(Integer informacao) {
		this.informacao = informacao;
	}

	public No getProximoNo() {
		return proximoNo;
	}

	public void setProximoNo(No proximoNo) {
		this.proximoNo = proximoNo;
	}

	
}
